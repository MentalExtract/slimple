var config = require('./config/app');
var slimple = require("slimple"); 

/**
 * Minimal app skeleton for slimple
 */ 
app = {

    /**
     * Initialize the context.
     * Everything defined here will be available in the events and services.
     */ 
    context : {
        customContextMethod : function(text){
            this.log.trace(text);
        } 
    },
    
    /**
     * Initialize the app
     */ 
    init: function(){
        this.context = slimple.context.build(config, this.context);
    },
    
    /**
     * Run the app
     */ 
    run: function(){
        this.init();
        slimple.server.httpd.start(this.context);
    }
    
};

app.run();
