module.exports = {
    
    app : app ? app : {}, // global scope from app.js
    
    // the resource where this service is available 
    resource: '(/:what)',
    
    /**
     * handler for GET request
     */ 
    GET: function(http){
       //http.response.setHeader('Content-Type','application/json');
        this.anotherServiceMethod(http);
        if(http.data.what === 'error'){
            throw { status: 500, message : new Error("blabal")};
        }
        http.reply(http.data);
    },
    
    /**
     * handler for POST request
     */
    POST: function(http){
        http.reply(http.data);
    },
    
    // possible handlers: GET, POST, PUT, DELETE
    
    /**
     *  a method to show some Slimple functionalities
     */ 
    anotherServiceMethod: function(http){
        
        // session variables
        var sessionId = http.session.uid();
        var counter = http.session.get('counter');
        counter = counter ? ++counter : 1;
        http.session.set('counter', counter);
        
        // logging: trace, debug, info, error
        app.context.log.trace(sessionId+" > counter = "+counter);
        
        // event emit
        app.context.event.emit("echoEvent", sessionId+" > "+http.request.method + " echoEvent: " + JSON.stringify(http.data));
        
        // user defined method in the context, see app.js
        app.context.customContextMethod(sessionId+" > "+"extended context method called from echo service");
    }
};
