var config = require('./config/app');
var slimple = require("slimple"); 

/**
 * minimal app skeleton
 */ 
var app = {

    /**
     * initialize the app context
     */ 
    context : {
        extCtxMth : function(text){
            this.log.trace(text); // methods from the context are available in the extended context
        } 
    },
    
    /**
     * initialize the app
     */ 
    init: function(){
        // build context
        this.context = slimple.contextFactory.build(config, this.context);
    },
    
    /**
     * run the app
     */ 
    run: function(){
        this.init();
        slimple.server.httpd.start(this.context);
    }
    
};

app.run();
