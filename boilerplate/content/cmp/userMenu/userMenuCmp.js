var userMenuConfig = {
    show: true
};
var userMenu = {
    name: 'userMenu',
    renderTo: 'rightPanel',
    staticData: userMenuConfig,
    template: '/cmp/userMenu/userMenuTemplate.ejs',
    listeners: {
    }
};