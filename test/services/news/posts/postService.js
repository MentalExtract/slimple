module.exports = {
    
    resource: '(/:postId)',

    GET: function(ctx, data, http){
        http.reply('posts ' + JSON.stringify(data));
    }
};
