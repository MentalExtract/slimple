module.exports = {
    port: process.env.VCAP_APP_PORT || process.env.PORT || 8080,
    serviceLocation: '/test/services',
    eventLocation: '/test/events',
    contentLocation: '/test/content',
    schemaLocation: '/test/schemas',
    db: require('./db'),
    log: require('./log')
};