var eventHandler = {

	/**
	* initialize the default event handlers
	*/
	init: function(){
	
		// handler for menu items
		$('.menuItem').on('click', function(event) {
			// if target is undefined, open in content div
			if(typeof $(this).attr("target") === 'undefined'){
				event.preventDefault();
				$.get(event.target, function(data) {
					$('#content').html(data);
					return false;
				});
			}
		});	

		
		// handler for form ajax calls with json data
		$('body').on('click', '.ajax', function(event) {
			event.preventDefault();
			var form = $(this).parents('form:first');
			// append the hidden source field if it doesn't exist in this form
			if(form.children('[name="source"]').length == 0){
				$('<input>').attr({
					type: 'hidden',
					name: 'source',
					value: form.attr('id')
				}).appendTo(form);
			}			
			// send ajax request
			$.ajax({
				url: form.attr('action'),
				type: form.attr('method'),
				data: form.serialize(),
				success: function(data) {
					// trigger jsonRecieved event on source element
					$('#'+$.parseJSON(data).source).trigger('jsonRecieved', $.parseJSON(data));
					return false;
				}
			});
		});
		
		// handler for form sync calls
		$('body').on('click', '.sync', function(event) {
			event.preventDefault();
			var form = $(this).parents('form:first');
			// send ajax request
			$.ajax({
				url: form.attr('action'),
				type: form.attr('method'),
				data: form.serialize(),
				success: function(data) {
					// render output to content div
					$('#content').html(data);
					return false;
				}
			});
		});
		
	},
    
    ajaxSubmit : function(form, callback){
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize(),
            success: function(){ callback(); }
        });
    }

}