module.exports = function(serviceHolder) {

    serviceHolder['/schemas/get'] = function(ctx, data, http) {
        return {
            GET: http.returnJson(ctx.db.getSchema(data.schema))
        };

    };

    serviceHolder['/schemas/getAll'] = function(ctx, data, http) {
        return {
            GET: http.returnJson({
                schemas: Object.keys(ctx.db.schemas)
            })
        };
    };
};