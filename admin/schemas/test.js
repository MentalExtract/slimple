module.exports = {
    name: 'TestSchema',
    schema : {
        field1 : { type : "String", required : true },
        field2 : { type : "String", required : true },
        field3 : { type : "String", required : true },
        field4 : { type : "String", required : true, default : "blablabla" }
    }
};