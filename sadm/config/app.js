/**
 * config file for the application.
 * don't expose this folder to the web, becuase it contains the server admin password
 */
 module.exports = {
    
    /* the port on which the app should run */
    port: process.env.VCAP_APP_PORT || process.env.PORT || 8080,
    
    /* locations for automatic loading */
    serviceLocation: '/sadm/services',
    eventLocation: '/sadm/events',
    contentLocation: '/sadm/content',
    schemaLocation: '/sadm/schemas',
    
    /* additional config files*/
    db: require('./db'),
    log: require('./log'),
    
    /* password for system administrator */
    // TODO make a login for server admin
    serverAdmin: {
        name: 'sa',
        password: 'sa'
    }
};