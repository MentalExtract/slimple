var fs = require('fs');

/**
 * FileSystemService
 */
module.exports = function(serviceHolder) {

    /**
     * list all files and directories of a given path
     */
    serviceHolder['/fs/:path'] = function(ctx, data, http) {
        return {
            GET: function() {

                try {
                    ctx.log.trace('listFolder: requested ' + data.path);
                    //if (data.path.indexOf('/') === 0) {
                        var fromPath = process.cwd() + ctx.config.contentLocation + '/'+ data.path;
                        ctx.log.trace("read path " + fromPath);
                        fs.stat(fromPath, function(err, stats) {
                            if (stats !== null && stats !== undefined) {
                                if (stats.isDirectory()) {
                                    fs.readdir(fromPath, function(err, files) {
                                        http.returnJson({
                                            folder: files.sort(),
                                            isFile: false
                                        });
                                    });
                                }
                                else {
                                    http.returnJson({
                                        folder: [],
                                        isFile: true
                                    });
                                }
                            }
                        });
                    //}
                } catch (error) {
                    ctx.log.error(error);
                    http.returnJson({
                        folder: [],
                        isFile: false,
                        msg: error
                    });
                }
            }
        };
    };
};
