module.exports = function(services) {

    services['/session/:attribute'] = function(ctx, data, http) {
        
        return {
            
            GET: function(){
                var result = data.attribute === 'id' ? http.session.uid() : http.session.get(data.attribute);
                http.returnJson({'value' : result});
            },
            
            PUT: function(){
                http.session.set(data.attribute, data.value);
                http.returnJson({success:true});
            }
            
        };
        
    };
    
};