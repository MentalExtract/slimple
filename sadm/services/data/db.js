//var crypto = require('crypto');

module.exports = function(services) {

    services['/db/:schema(/:id)'] = function(ctx, data, http) {


        var id = data.id;
        var inputData = data.data ? JSON.parse(data.data) : null;
        var query = inputData !== null && inputData.query ? JSON.parse(inputData.query) : null;
        var schema = data.schema;
        var schemaDefinition = ctx.db.getSchema(schema);
        
        return {
            
            /**
             * read
             */
            GET: function(){
                //ctx.db.perform(function(db) {
                    var model = ctx.db.model(schema);
                    if(id){ // read by id
                        model.findById(id, function(error, result) {
                            http.returnJson({data:result,schema:schemaDefinition, schemaName: schema});
                        });
                    } else if(query){ // find
                        for (var k in query) { query[k] = new RegExp(query[k], 'i'); }
                        model.find(query).execFind(function(error, result) {
                            result = result ? result.reverse() : {};
                            http.returnJson({data:result,schema:schemaDefinition, schemaName: schema}); // FIXME create a solution of last to oldest
                        });
                    }else { // read all
                        model.find().execFind(function(error, result) {
                            result = result ? result.reverse() : {};
                            http.returnJson({data:result,schema:schemaDefinition, schemaName: schema}); // FIXME create a solution of last to oldest
                        });
                    }
                //});
            },
            
            /**
             * create entry
             */
            POST: function(){
                
                ctx.db.perform(function(db) {
                    var Model = ctx.db.model(schema);
                    var preparedData = mapData(schemaDefinition, inputData, {});
                    var dbObject = new Model(preparedData);
                    dbObject.save(function(error) {
                        http.returnJson(error ? {msg:error} : { id : dbObject._id });
                    });
                }); 
            },
            
            /**
             * modify entry 
             */
            PUT: function(){
                if(id){
                    ctx.db.perform(function(db) {
                        var Model = ctx.db.model(schema);
                        Model.findOne({
                            _id: id
                        }, function(err, doc) {
                            var upd = mapData(schemaDefinition, inputData, doc);
                            upd.save(function(error) {
                                http.returnJson(error ? {msg:error} : {});
                            });
                        });
                    });
                } else {
                    http.returnJson({msg:'no id specified'});
                }
            },
            /**
             * delete entry
             */
            DELETE: function() {
                if(id){
                    ctx.db.perform(function(db) {
                        var model = ctx.db.model(schema);
                        model.remove({
                            _id: id
                        }, function(error) {
                            http.returnJson(error ? {msg:error} : {});
                        });
                    });
                } else {
                    http.returnJson({msg:'no id specified'});
                }
            }
            
        };

    };
    
    var mapData = function(schema, from, to) {
        delete from._id;
        delete from.schema;
        for (var key in from) {
            if (from[key] !== null && from[key] !== 'undefined') {
                /*if(schema[key].encrypted){ // TODO map of additional properties
                    if(!to[key]){ // encrypt only if field didn't containi any data yet (insert)
                        var md5 = crypto.createHash('md5').update(from[key]).digest("hex");
                        to[key] = md5;
                    }
                } else {*/
                    to[key] = from[key];
                //}
            }
        }
        return to;
    };
};