module.exports = function(serviceHolder) {

    serviceHolder['/schema/:name'] = function(ctx, data, http) {
        return {
            GET: http.returnJson({ schema : ctx.db.getSchema(data.name) })
        };

    };

    serviceHolder['/schema'] = function(ctx, data, http) {
        return {
            GET: http.returnJson({ schemas: Object.keys(ctx.db.schemas) })
        };
    };
};