var eventHandler = {

	/**
	* initialize the default event handlers
	*/
	init: function(){

        // default link handler
        /*$('body').on('click', 'a', function(event) {
            var that = this;
			// if target is undefined, open in content div
			if(typeof $(that).attr("target") === 'undefined'){
				event.preventDefault();
				$.get(event.target, function(data) {
					$('#content').html(data);
					return false;
				});
			}
		});	*/
        
        $('body').on('click', '.onPageNav', function(event) {
            event.preventDefault();
            eventHandler.scrollTo($(this).attr("href"));
            
		});	
        
        $('body').on('click', '.onPageLink', function(event) {
            event.preventDefault();
            $.get(event.target, function(data) {
                $('#content').html(data);
            });
            
    	});	
        
		// handler for menu items
		$('.menuItem').on('click', function(event) {
            var that = this;
			// if target is undefined, open in content div
			if(typeof $(that).attr("target") === 'undefined'){
				event.preventDefault();
				$.get(event.target, function(data) {
                    $('li').removeClass('active');
                    $(that).parent('li').addClass('active');
					$('#content').html(data);
                    if($(that).hasClass('scrollNav')){
                        eventHandler.scrollTo("#"+$(that).attr("href").split('#')[1]);
                    }
					return false;
				});
			}
		});	

		
		// handler for form ajax calls with json data
		$('body').on('click', '.ajax', function(event) {
			event.preventDefault();
			var form = $(this).parents('form:first');
			// append the hidden source field if it doesn't exist in this form
			if(form.children('[name="source"]').length == 0){
				$('<input>').attr({
					type: 'hidden',
					name: 'source',
					value: form.attr('id')
				}).appendTo(form);
			}			
			// send ajax request
			$.ajax({
				url: form.attr('action'),
				type: form.attr('method'),
				data: form.serialize(),
				success: function(data) {
					// trigger jsonRecieved event on source element
					$('#'+$.parseJSON(data).source).trigger('jsonRecieved', $.parseJSON(data));
					return false;
				}
			});
		});
		
		// handler for form sync calls
		$('body').on('click', '.sync', function(event) {
			event.preventDefault();
			var form = $(this).parents('form:first');
			// send ajax request
			$.ajax({
				url: form.attr('action'),
				type: form.attr('method'),
				data: form.serialize(),
				success: function(data) {
					// render output to content div
					$('#content').html(data);
					return false;
				}
			});
		});
		
	},
    scrollTo : function (id){
        $('html,body').animate({scrollTop: $(id).offset().top - 52},'slow');
        return false;
    }

}