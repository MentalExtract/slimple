/**
 * configuration for mongoDB 
 */
module.exports = {
    connectionString : '',
    connectionProperties:  { 
        server: { 
            poolSize: 10 
        } 
    }
};
